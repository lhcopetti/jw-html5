
function Inimigo(contexto, imagem) {
    this.contexto = contexto;
    this.y = 0;
    this.x = 10;
    this.imagem = imagem;
}

Inimigo.prototype.atualizar = function () {
    this.x += 1;
}

Inimigo.prototype.desenhar = function () {
    this.contexto.drawImage(this.imagem,
                       0, 0, 224, 128, // offset 
                       this.x, 50, // x, y
                       112, 62 // resize
                       );
}