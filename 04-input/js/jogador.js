
function Jogador(contexto, imagem, teclado) {
    this.contexto = contexto;
    this.y = 450;
    this.x = contexto.canvas.width / 2.0;
    this.imagem = imagem;
    this.teclado = teclado;
}

Jogador.prototype.atualizar = function () {

    if (this.teclado.pressionada(SETA_ESQUERDA))
        this.x -= 10;

    if (this.teclado.pressionada(SETA_DIREITA))
        this.x += 10;   
}

Jogador.prototype.desenhar = function () {
    this.contexto.drawImage(this.imagem,
                       0, 0, 343, 383, // offset 
                       this.x, this.y, // x, y
                       114, 127 // resize
                       );
}